

var BGimg = "url('/var/www/game/images/freetextures/TexturesCom_MetalRusted0130_2_S.jpg')";
var BGimg2 = "url('/var/www/game/images/assets/pillar.svg')";

var cubehight = '0';

var BGcolorX = 'green';
var BGcolorY = 'blue';
var cubefaceX = '0';
var cubefaceY = '0';
///////////////////////////////////// Front 1 ///////////////////////////////////// 
var cubefrontW = '600';
var cubefrontH = '200';
var Disf = 0;
var fronttX = '0';
var fronttY = '0';
var frontZ = '0';
var frontrt = '0';
///////////////////////////////////// Back 2 ///////////////////////////////////// 
var cubebackW = '0';
var cubebackH = '0';
var Disb = 0;
var backtX = '0';
var backtY = '0';
var backZ = '0';
var backrt = '0';
///////////////////////////////////// Right 3 ///////////////////////////////////// 
var cuberightW = '0';
var cuberightH = '0';
var Disr = 0;
var righttX = '0';
var righttY = '0';
var rightZ = '0';
var rightrt = '0';
///////////////////////////////////// Left 4 ///////////////////////////////////// may turn it back to 0 !!! 
var cubeleftW = '0';
var cubeleftH = '0';
var Disl = '0';
var lefttX = '0' ;
var lefttY = '0';
var leftZ = '0';
var leftrt = '0';
///////////////////////////////////// Top 5 ///////////////////////////////////// 
var cubetopW = '0';
var cubetopH = '0';
var Dist = 0;
var toptX = '0';
var toptY = '0';
var topZ = '0';
var toprt = '0';
///////////////////////////////////// Bottom 6 ///////////////////////////////////// 
var cubebottomW = '0';
var cubebottomH = '0';
var Disbo = 0;
var bottomtX = '0';
var bottomtY = '0';
var bottomZ = '0';
var bottomrt = '0';
///////////////////////////////////////////////////////////////////////////////////// 
var cubez = cubefaceX;
var objctH = '1px';
var objctW = '1px';
var objctX = '0';
var objctY = '0';
var objct_2dX = objctX + offsetx;
var objct_2dY = objctY + offsety;
var objctPer = '0';
var objctRy = '0';
var objctRx = '0';
var objctSkewX = '0';
var objctSkewY = '0';
var objctZ = '400';
var objcttX = '0';
var objcttY = '0';
var objct = '0';
var objctTOx = '0';
var objctTOy = '0';
var objctTOxy = '' + objctTOx + '' + objctTOy + '';






///////////////////////////////////////////////////////////////////////////////////// 
function renderObjnew() {
var $divcube = $("#obj1");
$divcube.append("");
$divcube.append("<div class='face front'>1</div>");
$divcube.append("<div class='face back'>2</div>");
$divcube.append("<div class='face right'>3</div>");
$divcube.append("<div class='face left'>4</div>");
$divcube.append("<div class='face top'>5</div>");
$divcube.append("<div class='face bottom'>6</div>");
$divcube.append("");
$divcube.append("");

};

var interval = setInterval(function() {
$("#obj1").css({ 'opacity':'1','z-index':'1','position': 'absolute','background-color': '','height': objctH, 'width': objctW,'left': objctX,'top':objctY,'transform-style': 'preserve-3d',  'transform-origin': ''  + objctTOx + ',' + objctTOy + '', '-webkit-transform': 'perspective(' + objctPer + 'px) scale3d(' + sclx +',' + scly +',' + sclz +') rotateY(' + objctRy + 'deg) rotateX(' + objctRx + 'deg) skewX(' + objctSkewX + 'deg) skewY(' + objctSkewY + 'deg)  translate3d(' + objcttX + 'px,' + objcttY + 'px,' + objctZ + 'px)rotate(' + objct + 'deg)'});
    $("#obj1_2d").css({ 'opacity':'1','z-index':'1','position': 'absolute','background-color': 'black','height': objctH, 'width': objctW,'left': ''+ objct_2dX +'px','top':''+ objct_2dY +'px','transform-style': 'preserve-3d',  'transform-origin': ''  + objctTOx + ',' + objctTOy + '', '-webkit-transform': 'perspective(' + objctPer + 'px) rotateY(' + objctRy + 'deg) rotateX(' + objctRx + 'deg) skewX(' + objctSkewX + 'deg) skewY(' + objctSkewY + 'deg) rotate(' + objct + 'deg)'});

$(".face").css({ 'opacity':'1','display': 'block','margin-top':'50%','position': 'absolute','width': cubefaceX,'height': cubefaceY,'border': 'none','line-height': '100px','font-family': 'sans-serif','font-size': '60px','color': 'white','text-align': 'center', 'transform': 'rotateY(90deg) translateZ('+Disb+'px)'});
$(".front").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cubefrontW,'height': cubefrontH,'background-color': 'green','background-image': '' + BGimg + '','background-size': '100% 100%','transform': 'translateZ('+Disf+'px) translate3d(' + fronttX + 'px,' + fronttY + 'px,' + frontZ + 'px)rotate(' + frontrt + 'deg)'});
$(".back").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cubebackW,'height': cubebackH,'background-color': 'red','background-image': '' + BGimg + '','background-size': '100% 100%', 'transform': 'rotateY(90deg) translateZ('+Disb+'px) translate3d(' + backtX + 'px,' + backtY + 'px,' + backZ + 'px)rotate(' + backrt + 'deg)'});
$(".right").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cuberightW,'height': cuberightH,'background-color': 'blue','background-image': '' + BGimg + '','background-size': '100% 100%','transform': 'rotateY(-180deg) translateZ('+Disr+'px) translate3d(' + righttX + 'px,' + righttY + 'px,' + rightZ + 'px)rotate(' + rightrt + 'deg)'});
$(".left").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cubeleftW,'height': cubeleftH,'background-color': 'purple','background-image': '' + BGimg + '','background-size': '100% 100%','transform': 'rotateX(-90deg) translateZ('+Disl+'px) translate3d(' + lefttX + 'px,' + lefttY + 'px,' + leftZ + 'px)rotate(' + leftrt + 'deg)'});
$(".top").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cubetopW,'height': cubetopH,'background-color': 'pink','background-image': '' + BGimg + '','background-size': '100% 100%','transform': 'rotateX(90deg) translateZ('+Dist+'px) translate3d(' + toptX + 'px,' + toptY + 'px,' + topZ + 'px)rotate(' +toprt + 'deg)'});
$(".bottom").css({ 'opacity':'1','perspective-origin': '150% 150%','width': cubebottomW,'height': cubebottomH,'background-color': 'cyan','background-image': '' + BGimg + '','background-size': '100% 100%','transform': 'rotateY(-90deg) translateZ('+Disbo+'px) translate3d(' + bottomtX + 'px,' + bottomtY + 'px,' +bottomZ + 'px)rotate(' +bottomrt + 'deg)'});





}, refreshStairs);
