function toolGeoShapeCreate() {
var $toolCreate = $("#geoshapeC");
$toolCreate.append("<table id='TBsphere' class='darkTable' style='width: 100%; border='0' facepadding='0'><tbody><tr><td><button id='geotype1'>Plane</button></td><td><button id='geotype2'>Cube</button></td><td><button id='geotype3'>Circle</button></td><td><button id='geotype4'>UV Sphere</button></td></tr></tbody></table>");
$( "#geotype1" ).click(function() { $( "#toolboxTransform" ).toggle(); });
$( "#geotype2" ).click(function() { document.getElementById('divcube').appendChild(createModel(faceS_PER_CIRCLE = CUBE)); });
$( "#geotype3" ).click(function() { $( "#toolboxTransform" ).toggle(); });
$( "#geotype4" ).click(function() {  document.getElementById('divsphere').appendChild(createModel(faceS_PER_CIRCLE = SPHERE)); FindChildrenNamesID();});
$( "#geotype5" ).click(function() { $( "#toolboxTransform" ).toggle(); });
};

