function toolVectors() {
html = ''
+'\
<table id="TBsphere" class="darkTable" style="width: 100%;" border="0" polypadding="0"><tbody>\
<tr>\
<td><select id="polyOPT"></select></td>\
<td></td>\
<td></td>\
</tr>\
<tr>\
<td><div id="slidecontainer20"><input type="range" min="0" max="2000" value="210" class="slider" id="polySizeX"></div></td>\
<td><p>SizeX:</p></td>\
<td><span id="polySizeXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer20"><input type="range" min="0" max="2000" value="500" class="slider" id="polySizeY"></div></td>\
<td><p>SizeY:</p></td>\
<td><span id="polySizeYout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer12"><input type="range" min="-1000" max="1000" value="400" class="slider" id="polyX"></div></td>\
<td><p>Position X:</p></td>\
<td><span id="polyXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer13"><input type="range" min="-1000" max="1000" value="400" class="slider" id="polyY"></div></td>\
<td><p>Position Y:</p></td>\
<td><span id="polyYout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="-1000" max="1000" value="400" class="slider" id="polyZ"></div></td>\
<td><p>Position Z:</p></td>\
<td><span id="polyZout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer15"><input type="range" min="0" max="360" value="0" class="slider" id="polyR"></div></td>\
<td><p>Rotate:</p></td>\
<td><span id="polyRout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer16"><input type="range" min="0" max="180" value="0" class="slider" id="polyRX"></div></td>\
<td><p>Rotate X:</p></td>\
<td><span id="polyRXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer17"><input type="range" min="0" max="180" value="0" class="slider" id="polyRY"></div></td>\
<td><p>Rotate Y:</p></td>\
<td><span id="polyRYout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer18"><input type="range" min="0" max="200" value="0" class="slider" id="polySX"></div></td>\
<td><p>SkewX:</p></td>\
<td><span id="polySXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer20"><input type="range" min="0" max="200" value="0" class="slider" id="polySY"></div></td>\
<td><p>SkewY:</p></td>\
<td><span id="polySYout"></span></td>\
</tr>\
<tr>\
<tr>\
<td><div id="slidecontainer31"><input type="range" min="0" max="2000" value="100" class="slider" id="polypx1"></div></td>\
<td><p id="PX1-trigger">PX1:</p></td>\
<td><span id="polypx1out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer32"><input type="range" min="0" max="2000" value="10" class="slider" id="polypy1"></div></td>\
<td><p id="PY1-trigger">PY1:</p></td>\
<td><span id="polypY1out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer31"><input type="range" min="0" max="2000" value="40" class="slider" id="polypx2"></div></td>\
<td><p id="PX2-trigger">PX2:</p></td>\
<td><span id="polypx2out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer32"><input type="range" min="0" max="2000" value="198" class="slider" id="polypy2"></div></td>\
<td><p id="PY2-trigger">PY2:</p></td>\
<td><span id="polypY2out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer31"><input type="range" min="0" max="2000" value="190" class="slider" id="polypx3"></div></td>\
<td><p id="PX3-trigger">PX3:</p></td>\
<td><span id="polypx3out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer32"><input type="range" min="0" max="2000" value="78" class="slider" id="polypy3"></div></td>\
<td><p id="PY3-trigger">PY3:</p></td>\
<td><span id="polypY3out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer31"><input type="range" min="0" max="2000" value="10" class="slider" id="polypx4"></div></td>\
<td><p id="PX4-trigger">PX4:</p></td>\
<td><span id="polypx4out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer42"><input type="range" min="0" max="2000" value="78" class="slider" id="polypy4"></div></td>\
<td><p id="PY4-trigger">PY4:</p></td>\
<td><span id="polypY4out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer31"><input type="range" min="0" max="2000" value="160" class="slider" id="polypx5"></div></td>\
<td><p id="PX5-trigger">PX5:</p></td>\
<td><span id="polypx5out"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer52"><input type="range" min="0" max="2000" value="198" class="slider" id="polypy5"></div></td>\
<td><p id="PY5-trigger">PY5:</p></td>\
<td><span id="polypY5out"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menupolygon').html(html);
var polySIX = document.getElementById("polySizeX");
var outputpolSX = document.getElementById("polySizeXout");
outputpolSX.innerHTML = polySIX.value;
polySIX.oninput = function() {
  outputpolSX.innerHTML = this.value;
polygonW = outputpolSX.innerHTML;
}
var polySIY = document.getElementById("polySizeY");
var outputpolSY = document.getElementById("polySizeYout");
outputpolSY.innerHTML = polySIY.value;
polySIY.oninput = function() {
  outputpolSY.innerHTML = this.value;
polygonH = outputpolSY.innerHTML;
}


var polyPX = document.getElementById("polyX");
var outputpolyX = document.getElementById("polyXout");
outputpolyX.innerHTML = polyPX.value;
polyPX.oninput = function() {
  outputpolyX.innerHTML = this.value;
polygonX = outputpolyX.innerHTML;
}
var polyPY = document.getElementById("polyY");
var outputpolyY = document.getElementById("polyYout");
outputpolyY.innerHTML = polyPY.value;
polyPY.oninput = function() {
  outputpolyY.innerHTML = this.value;
polygonY = outputpolyY.innerHTML;
}
var polyPZ = document.getElementById("polyZ");
var outputpolyZ = document.getElementById("polyZout");
outputpolyZ.innerHTML = polyPZ.value;
polyPZ.oninput = function() {
  outputpolyZ.innerHTML = this.value;
polygonZ = outputpolyZ.innerHTML;
}
var polyPR = document.getElementById("polyR");
var outputpolyR = document.getElementById("polyRout");
outputpolyR.innerHTML = polyPR.value;
polyPR.oninput = function() {
  outputpolyR.innerHTML = this.value;
polygon = outputpolyR.innerHTML;
}
var polyPRX = document.getElementById("polyRX");
var outputpolyRX = document.getElementById("polyRXout");
outputpolyRX.innerHTML = polyPRX.value;
polyPRX.oninput = function() {
  outputpolyRX.innerHTML = this.value;
polygonRx = outputpolyRX.innerHTML;
}
var polyPRY = document.getElementById("polyRY");
var outputpolyRY = document.getElementById("polyRYout");
outputpolyRY.innerHTML = polyPRY.value;
polyPRY.oninput = function() {
  outputpolyRY.innerHTML = this.value;
polygonRy = outputpolyRY.innerHTML;
}

var polySKX = document.getElementById("polySX");
var outputpolySKX = document.getElementById("polySXout");
outputpolySKX.innerHTML = polySKX.value;
polySKX.oninput = function() {
  outputpolySKX.innerHTML = this.value;
polygonSkewX = outputpolySKX.innerHTML;
}
var polySKY = document.getElementById("polySY");
var outputpolySKY = document.getElementById("polySYout");
outputpolySKY.innerHTML = polySKY.value;
polySKY.oninput = function() {
  outputpolySKY.innerHTML = this.value;
polygonSkewY = outputcellSKY.innerHTML;
}
var polyPX1 = document.getElementById("polypx1");
var outputpolypX1 = document.getElementById("polypx1out");
outputpolypX1.innerHTML = polyPX1.value;
polyPX1.oninput = function() {
  outputpolypX1.innerHTML = this.value;
px1 = outputpolypX1.innerHTML;
}
var polyPY1 = document.getElementById("polypy1");
var outputpolypY1 = document.getElementById("polypY1out");
outputpolypY1.innerHTML = polyPY1.value;
polyPY1.oninput = function() {
  outputpolypY1.innerHTML = this.value;
py1 = outputpolypY1.innerHTML;
}
var polyPX2 = document.getElementById("polypx2");
var outputpolypX2 = document.getElementById("polypx2out");
outputpolypX2.innerHTML = polyPX2.value;
polyPX2.oninput = function() {
  outputpolypX2.innerHTML = this.value;
px2 = outputpolypX2.innerHTML;
}
var polyPY2 = document.getElementById("polypy2");
var outputpolypY2 = document.getElementById("polypY2out");
outputpolypY2.innerHTML = polyPY2.value;
polyPY2.oninput = function() {
  outputpolypY2.innerHTML = this.value;
py2 = outputpolypY2.innerHTML;
}
var polyPX3 = document.getElementById("polypx3");
var outputpolypX3 = document.getElementById("polypx3out");
outputpolypX3.innerHTML = polyPX3.value;
polyPX3.oninput = function() {
  outputpolypX3.innerHTML = this.value;
px3 = outputpolypX3.innerHTML;
}
var polyPY3 = document.getElementById("polypy3");
var outputpolypY3 = document.getElementById("polypY3out");
outputpolypY3.innerHTML = polyPY3.value;
polyPY3.oninput = function() {
  outputpolypY3.innerHTML = this.value;
py3 = outputpolypY1.innerHTML;
}

var polyPX4 = document.getElementById("polypx4");
var outputpolypX4 = document.getElementById("polypx4out");
outputpolypX4.innerHTML = polyPX4.value;
polyPX4.oninput = function() {
  outputpolypX4.innerHTML = this.value;
px4 = outputpolypX4.innerHTML;
}
var polyPY4 = document.getElementById("polypy4");
var outputpolypY4 = document.getElementById("polypY4out");
outputpolypY4.innerHTML = polyPY4.value;
polyPY4.oninput = function() {
  outputpolypY4.innerHTML = this.value;
py4 = outputpolypY1.innerHTML;
}
var polyPX5 = document.getElementById("polypx5");
var outputpolypX5 = document.getElementById("polypx5out");
outputpolypX5.innerHTML = polyPX5.value;
polyPX5.oninput = function() {
  outputpolypX5.innerHTML = this.value;
px5 = outputpolypX5.innerHTML;
}
var polyPY5 = document.getElementById("polypy5");
var outputpolypY5 = document.getElementById("polypY5out");
outputpolypY5.innerHTML = polyPY5.value;
polyPY5.oninput = function() {
  outputpolypY5.innerHTML = this.value;
py5 = outputpolypY1.innerHTML;
}
};