var CHred = '0';
var CHgreen = '0';
var CHblue = '0';
var CHopacity = '0';
var CHredB = '255';
var CHgreenB = '0';
var CHblueB = '0';
var CHopacityB = '1';
var CHredF = '255';
var CHgreenF = '0';
var CHblueF = '0';
var CHopacityF = '1';
var CHredSouter = '255';
var CHgreenSouter = '255';
var CHblueSouter = '255';
var CHopacitySouter = '1';
var CHopacitySouter = '255';
var CHredSinner = '255';
var CHgreenSinner = '255';
var CHblueSinner = '255';
var CHopacitySinner = '1';
function toolColorPick() {
  /*
html = ''
+'\
<table id="TBsphere" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer12"><input type="range" min="0" max="255" value="0" class="slider" id="Cred"></div></td>\
<td><p>Red:</p></td>\
<td><span id="redout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer13"><input type="range" min="0" max="255" value="0" class="slider" id="Cgreen"></div></td>\
<td><p>Green:</p></td>\
<td><span id="greenout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="255" value="0" class="slider" id="Cblue"></div></td>\
<td><p>Blue:</p></td>\
<td><span id="blueout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="100" value="0" class="slider" id="Copacity"></div></td>\
<td><p>Opacity:</p></td>\
<td><span id="opacityout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menucolorSface').html(html);


var redPX = document.getElementById("Cred");
var outputred = document.getElementById("redout");
outputred.innerHTML = redPX.value;
redPX.oninput = function() {
  outputred.innerHTML = this.value;
CHred = outputred.innerHTML;
}
var greenPY = document.getElementById("Cgreen");
var outputgreen = document.getElementById("greenout");
outputgreen.innerHTML = greenPY.value;
greenPY.oninput = function() {
  outputgreen.innerHTML = this.value;
CHgreen = outputgreen.innerHTML;
}
var bluePZ = document.getElementById("Cblue");
var outputblue = document.getElementById("blueout");
outputblue.innerHTML = bluePZ.value;
bluePZ.oninput = function() {
  outputblue.innerHTML = this.value;
CHblue = outputblue.innerHTML;
}
var opacityPZ = document.getElementById("Copacity");
var outputopacity = document.getElementById("opacityout");
outputopacity.innerHTML = opacityPZ.value/100;
opacityPZ.oninput = function() {
  outputopacity.innerHTML = this.value/100;
CHopacity = outputopacity.innerHTML;
}
*/
html = ''
+'\
<table id="TBsphere" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer12"><input type="range" min="0" max="255" value="255" class="slider" id="CredB"></div></td>\
<td><p>Red:</p></td>\
<td><span id="redBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer13"><input type="range" min="0" max="255" value="0" class="slider" id="CgreenB"></div></td>\
<td><p>Green:</p></td>\
<td><span id="greenBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="255" value="0" class="slider" id="CblueB"></div></td>\
<td><p>Blue:</p></td>\
<td><span id="blueBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="100" value="100" class="slider" id="CopacityB"></div></td>\
<td><p>Opacity:</p></td>\
<td><span id="opacityBout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menucolorSborder').html(html);


var redBPX = document.getElementById("CredB");
var outputredB = document.getElementById("redBout");
outputredB.innerHTML = redBPX.value;
redBPX.oninput = function() {
  outputredB.innerHTML = this.value;
CHredB = outputredB.innerHTML;
}
var greenBPY = document.getElementById("CgreenB");
var outputgreenB = document.getElementById("greenBout");
outputgreenB.innerHTML = greenBPY.value;
greenBPY.oninput = function() {
  outputgreenB.innerHTML = this.value;
CHgreenB = outputgreenB.innerHTML;
}
var blueBPZ = document.getElementById("CblueB");
var outputblueB = document.getElementById("blueBout");
outputblueB.innerHTML = blueBPZ.value;
blueBPZ.oninput = function() {
  outputblueB.innerHTML = this.value;
CHblueB = outputblueB.innerHTML;
}
var opacityBPZ = document.getElementById("CopacityB");
var outputopacityB = document.getElementById("opacityBout");
outputopacityB.innerHTML = opacityBPZ.value/100;
opacityBPZ.oninput = function() {
  outputopacityB.innerHTML = this.value/100;
CHopacityB = outputopacityB.innerHTML;
}
html = ''
+'\
<table id="TBsphere" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer12"><input type="range" min="0" max="255" value="255" class="slider" id="CredB"></div></td>\
<td><p>Red:</p></td>\
<td><span id="redBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer13"><input type="range" min="0" max="255" value="0" class="slider" id="CgreenB"></div></td>\
<td><p>Green:</p></td>\
<td><span id="greenBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="255" value="0" class="slider" id="CblueB"></div></td>\
<td><p>Blue:</p></td>\
<td><span id="blueBout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="0" max="100" value="100" class="slider" id="CopacityB"></div></td>\
<td><p>Opacity:</p></td>\
<td><span id="opacityBout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menucolorSfaces').html(html);


var redBPX = document.getElementById("CredB");
var outputredB = document.getElementById("redBout");
outputredB.innerHTML = redBPX.value;
redBPX.oninput = function() {
  outputredB.innerHTML = this.value;
CHredF = outputredB.innerHTML;
}
var greenBPY = document.getElementById("CgreenB");
var outputgreenB = document.getElementById("greenBout");
outputgreenB.innerHTML = greenBPY.value;
greenBPY.oninput = function() {
  outputgreenB.innerHTML = this.value;
CHgreenF = outputgreenB.innerHTML;
}
var blueBPZ = document.getElementById("CblueB");
var outputblueB = document.getElementById("blueBout");
outputblueB.innerHTML = blueBPZ.value;
blueBPZ.oninput = function() {
  outputblueB.innerHTML = this.value;
CHblueF = outputblueB.innerHTML;
}
var opacityBPZ = document.getElementById("CopacityB");
var outputopacityB = document.getElementById("opacityBout");
outputopacityB.innerHTML = opacityBPZ.value/100;
opacityBPZ.oninput = function() {
  outputopacityB.innerHTML = this.value/100;
CHopacityF = outputopacityB.innerHTML;
}

html = ''
+'\
<table id="TBsphereS" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer102"><input type="range" min="0" max="255" value="255" class="slider" id="CredSouter"></div></td>\
<td><p>Red:</p></td>\
<td><span id="redSouterout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer103"><input type="range" min="0" max="255" value="255" class="slider" id="CgreenSouter"></div></td>\
<td><p>Green:</p></td>\
<td><span id="greenSouterout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer104"><input type="range" min="0" max="255" value="255" class="slider" id="CblueSouter"></div></td>\
<td><p>Blue:</p></td>\
<td><span id="blueSouterout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer104"><input type="range" min="0" max="100" value="100" class="slider" id="CopacitySouter"></div></td>\
<td><p>Opacity:</p></td>\
<td><span id="opacitySouterout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menucolorSskin').html(html);


var redSouterPX = document.getElementById("CredSouter");
var outputredSouter = document.getElementById("redSouterout");
outputredSouter.innerHTML = redSouterPX.value;
redSouterPX.oninput = function() {
  outputredSouter.innerHTML = this.value;
CHredSouter = outputredSouter.innerHTML;
}
var greenSouterPY = document.getElementById("CgreenSouter");
var outputgreenSouter = document.getElementById("greenSouterout");
outputgreenSouter.innerHTML = greenSouterPY.value;
greenSouterPY.oninput = function() {
  outputgreenSouter.innerHTML = this.value;
CHgreenSouter = outputgreenSouter.innerHTML;
}
var blueSouterPZ = document.getElementById("CblueSouter");
var outputblueSouter = document.getElementById("blueSouterout");
outputblueSouter.innerHTML = blueSouterPZ.value;
blueSouterPZ.oninput = function() {
  outputblueSouter.innerHTML = this.value;
CHblueSouter = outputblueSouter.innerHTML;
}
var opacitySouterPZ = document.getElementById("CopacitySouter");
var outputopacitySouter = document.getElementById("opacitySouterout");
outputopacitySouter.innerHTML = opacitySouterPZ.value/100;
opacitySouterPZ.oninput = function() {
  outputopacitySouter.innerHTML = this.value/100;
CHopacitySouter = outputopacitySouter.innerHTML;
}

html = ''
+'\
<table id="TBsphereS" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer102"><input type="range" min="0" max="255" value="255" class="slider" id="CredSinner"></div></td>\
<td><p>Red:</p></td>\
<td><span id="redSinnerout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer103"><input type="range" min="0" max="255" value="255" class="slider" id="CgreenSinner"></div></td>\
<td><p>Green:</p></td>\
<td><span id="greenSinnerout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer104"><input type="range" min="0" max="255" value="255" class="slider" id="CblueSinner"></div></td>\
<td><p>Blue:</p></td>\
<td><span id="blueSinnerout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer104"><input type="range" min="0" max="100" value="100" class="slider" id="CopacitySinner"></div></td>\
<td><p>Opacity:</p></td>\
<td><span id="opacitySinnerout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#menucolorSskout').html(html);


var redSinnerPX = document.getElementById("CredSinner");
var outputredSinner = document.getElementById("redSinnerout");
outputredSinner.innerHTML = redSinnerPX.value;
redSinnerPX.oninput = function() {
  outputredSinner.innerHTML = this.value;
CHredSinner = outputredSinner.innerHTML;
}
var greenSinnerPY = document.getElementById("CgreenSinner");
var outputgreenSinner = document.getElementById("greenSinnerout");
outputgreenSinner.innerHTML = greenSinnerPY.value;
greenSinnerPY.oninput = function() {
  outputgreenSinner.innerHTML = this.value;
CHgreenSinner = outputgreenSinner.innerHTML;
}
var blueSinnerPZ = document.getElementById("CblueSinner");
var outputblueSinner = document.getElementById("blueSinnerout");
outputblueSinner.innerHTML = blueSinnerPZ.value;
blueSinnerPZ.oninput = function() {
  outputblueSinner.innerHTML = this.value;
CHblueSinner = outputblueSinner.innerHTML;
}
var opacitySinnerPZ = document.getElementById("CopacitySinner");
var outputopacitySinner = document.getElementById("opacitySinnerout");
outputopacitySinner.innerHTML = opacitySinnerPZ.value/100;
opacitySinnerPZ.oninput = function() {
  outputopacitySinner.innerHTML = this.value/100;
CHopacitySinner = outputopacitySinner.innerHTML;
}
};