var cubeDivH = '0px';
var cubeDivW = '0px';
var cubeDivX = '0';
var cubeDivY = '0';
var cubeDivPer = '0';
var cubeDivRy = '0';
var cubeDivRx = '0';
var cubeDivSkewX = '0';
var cubeDivSkewY = '0';
var cubeDivZ = '0';
var cubeDivtX = '0';
var cubeDivtY = '0';
var cubeDiv = '0';
var cubeDivTOx = '0';
var cubeDivTOy = '0';
var cubeDivTOxy = '' + cubeDivTOx + '' + cubeDivTOy + '';
function toolEditCube() {
var $toolcube = $("#menucube");
$toolcube.append("<table id='TBsphere' class='darkTable' style='width: 100%; border='2' facepadding='0'><tbody><tr><td><button id='toolcubeTools'>Tools</button></td><td><button id='toolcubeStyle'>Style</button></td></tr></tbody></table>\
<div id='objinfocube'></div>");
$("#spherestyle").css({  'display':'none','color': '#E6E6E6','opacity': '1', 'position': 'absolute','overflow': 'hidden','background-color': '#191919','height': '100%', 'width': '100%','right': '0%','top':'0%' });
$("#objinfocube").css({  'color': 'pink','opacity': '1', 'position': 'absolute','overflow': 'hidden','background-color': 'pink','height': '90%', 'width': '100%','right': '0%','top':'10%' });
var $toolinfoC = $("#objinfocube");
$toolinfoC.append("<div id='cubeTools'></div>");
$("#cubeTools").css({  'display':'none','color': 'white','opacity': '1', 'position': 'absolute','overflow': 'hidden','background-color': 'orange','height': '100%', 'width': '100%','right': '0%','top':'0%'  });
$("#toolcubeTools").click(function() { $( "#cubeTools" ).show(); $( "#cubeStyle" ).hide();});


$toolinfoC.append("<div id='cubeStyle'></div>");
$("#cubeStyle").css({  'display':'none','color': 'white','opacity': '1', 'position': 'absolute','overflow': 'hidden','background-color': 'white','height': '100%', 'width': '100%','right': '0%','top':'0%'  });
$("#toolcubeStyle").click(function() { $( "#cubeStyle" ).show(); $( "#cubeTools" ).hide();});






var interval = setInterval(function() {
$("#divcube").css({ 'opacity':'1','z-index':'1','position': 'absolute','background-color': 'pink','left': cubeDivX,'top':cubeDivY, 'height': cubeDivH, 'width': cubeDivW,'transform-style': 'preserve-3d',  'transform-origin': ''  + cubeDivTOx + ',' + cubeDivTOy + '', '-webkit-transform': 'perspective(' + cubeDivPer + 'px) scale(' + scl +') rotateY(' + cubeDivRy + 'deg) rotateX(' + cubeDivRx + 'deg) skewX(' + cubeDivSkewX + 'deg) skewY(' + cubeDivSkewY + 'deg)  translate3d(' + cubeDivtX + 'px,' + cubeDivtY + 'px,' + cubeDivZ + 'px)rotate(' + cubeDiv + 'deg)'});
}, refreshStairs);
var $toolcubePosRot = $("#cubeTools");
$toolcubePosRot.append("<table id='TBsphere' class='darkTable' style='width: 100%;' border='0' facepadding='0'><tbody><tr><td><div id='slidecontainer12'><input type='range' min='-20000' max='20000' value='400' class='slider' id='chpX'></div></td><td><p>Position X:</p></td><td><text id='chpXout'></text></td></tr><tr><td><div id='slidecontainer13'><input type='range' min='-20000' max='20000' value='400' class='slider' id='chpY'></div></td><td><p>Position Y:</p></td><td><span id='chpYout'></span></td></tr><tr><td><div id='slidecontainer14'><input type='range' min='-20000' max='20000' value='400' class='slider' id='chpZ'></div></td><td><p>Position Z:</p></td><td><span id='chpZout'></span></td></tr><tr><td><div id='slidecontainer15'><input type='range' min='0' max='360' value='0' class='slider' id='chpR'></div></td><td><p>Rotate:</p></td><td><span id='chpRout'></span></td></tr><tr><td><div id='slidecontainer16'><input type='range' min='0' max='360' value='0' class='slider' id='chpRX'></div></td><td><p>Rotate X:</p></td><td><span id='chpRXout'></span></td></tr><tr><td><div id='slidecontainer17'><input type='range' min='0' max='360' value='0' class='slider' id='chpRY'></div></td><td><p>Rotate Y:</p></td><td><span id='chpRYout'></span></td></table></tbody>");
var chpPX = document.getElementById("chpX");
var outputchpX = document.getElementById("chpXout");
outputchpX.innerHTML = chpPX.value;
chpPX.oninput = function() {
  outputchpX.innerHTML = this.value;
cubeDivX = outputchpX.innerHTML;
}
var chpPY = document.getElementById("chpY");
var outputchpY = document.getElementById("chpYout");
outputchpY.innerHTML = chpPY.value;
chpPY.oninput = function() {
  outputchpY.innerHTML = this.value;
cubeDivY = outputchpY.innerHTML;
}
var chpPZ = document.getElementById("chpZ");
var outputchpZ = document.getElementById("chpZout");
outputchpZ.innerHTML = chpPZ.value;
chpPZ.oninput = function() {
  outputchpZ.innerHTML = this.value;
cubeDivZ = outputchpZ.innerHTML;
}
var chpPR = document.getElementById("chpR");
var outputchpR = document.getElementById("chpRout");
outputchpR.innerHTML = chpPR.value;
chpPR.oninput = function() {
  outputchpR.innerHTML = this.value;
cubeDiv = outputchpR.innerHTML;
}
var chpPRX = document.getElementById("chpRX");
var outputchpRX = document.getElementById("chpRXout");
outputchpRX.innerHTML = chpPRX.value;
chpPRX.oninput = function() {
  outputchpRX.innerHTML = this.value;
cubeDivRx = outputchpRX.innerHTML;
}
var chpPRY = document.getElementById("chpRY");
var outputchpRY = document.getElementById("chpRYout");
outputchpRY.innerHTML = chpPRY.value;
chpPRY.oninput = function() {
  outputchpRY.innerHTML = this.value;
cubeDivRy = outputchpRY.innerHTML;
}
document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
  $('#divcube').bind('mousedown', function(e){
    $('#base').bind('mousemove', function(e){
      if(e.which == 1) {
      e.preventDefault();
        cubeDivX = e.pageX - window.innerWidth/2;
        cubeDivY = e.pageY - window.innerHeight/2;
      } if (e.which == 3 ) {
       e.preventDefault();
        cubeDivZ = -e.pageY + window.innerHeight/2;
      }  else if (controlPressed == false || shiftPressed == false) {
      	//e.stoppropagation();
      }
      });
      $('#divcube').bind('mouseup',function(){
        $('#base').unbind('mousemove')
      });
});
};


