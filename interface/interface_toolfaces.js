function toolFaces() {
html = ''
+'\
<select id="faceOPT"></select><button id="oPT4" onclick="GetChildrenStyle()">Style</button><button id="oPT5" onclick="GetChildrenStyle()">Remove</button><button id="oPT6">Color</button>\
<table id="TBsphere" class="darkTable" style="width: 100%;" border="0" facepadding="0"><tbody>\
<tr>\
<td><div id="slidecontainer12"><input type="range" min="-1000" max="1000" value="400" class="slider" id="faceX"></div></td>\
<td><p>Position X:</p></td>\
<td><span id="faceXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer13"><input type="range" min="-1000" max="1000" value="400" class="slider" id="faceY"></div></td>\
<td><p>Position Y:</p></td>\
<td><span id="faceYout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer14"><input type="range" min="-1000" max="1000" value="400" class="slider" id="faceZ"></div></td>\
<td><p>Position Z:</p></td>\
<td><span id="faceZout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer15"><input type="range" min="0" max="360" value="0" class="slider" id="faceR"></div></td>\
<td><p>Rotate:</p></td>\
<td><span id="faceRout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer16"><input type="range" min="0" max="180" value="0" class="slider" id="faceRX"></div></td>\
<td><p>Rotate X:</p></td>\
<td><span id="faceRXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer17"><input type="range" min="0" max="180" value="0" class="slider" id="faceRY"></div></td>\
<td><p>Rotate Y:</p></td>\
<td><span id="faceRYout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer18"><input type="range" min="0" max="200" value="0" class="slider" id="faceSX"></div></td>\
<td><p>SkewX:</p></td>\
<td><span id="faceSXout"></span></td>\
</tr>\
<tr>\
<td><div id="slidecontainer19"><input type="range" min="0" max="200" value="0" class="slider" id="faceSY"></div></td>\
<td><p>SkewY:</p></td>\
<td><span id="faceSYout"></span></td>\
</tr>\
</tbody>\
</table>';
$('#SphereEditFaces').html(html);
var facePX = document.getElementById("faceX");
var outputfaceX = document.getElementById("faceXout");
outputfaceX.innerHTML = facePX.value;
facePX.oninput = function() {
  outputfaceX.innerHTML = this.value;
faceX = outputfaceX.innerHTML;
}
var facePY = document.getElementById("faceY");
var outputfaceY = document.getElementById("faceYout");
outputfaceY.innerHTML = facePY.value;
facePY.oninput = function() {
  outputfaceY.innerHTML = this.value;
faceY = outputfaceY.innerHTML;
}
var facePZ = document.getElementById("faceZ");
var outputfaceZ = document.getElementById("faceZout");
outputfaceZ.innerHTML = facePZ.value;
facePZ.oninput = function() {
  outputfaceZ.innerHTML = this.value;
faceZ = outputfaceZ.innerHTML;
}
var facePR = document.getElementById("faceR");
var outputfaceR = document.getElementById("faceRout");
outputfaceR.innerHTML = facePR.value;
facePR.oninput = function() {
  outputfaceR.innerHTML = this.value;
facert = outputfaceR.innerHTML;
}
var facePRX = document.getElementById("faceRX");
var outputfaceRX = document.getElementById("faceRXout");
outputfaceRX.innerHTML = facePRX.value;
facePRX.oninput = function() {
  outputfaceRX.innerHTML = this.value;
faceRx = outputfaceRX.innerHTML;
}
var facePRY = document.getElementById("faceRY");
var outputfaceRY = document.getElementById("faceRYout");
outputfaceRY.innerHTML = facePRY.value;
facePRY.oninput = function() {
  outputfaceRY.innerHTML = this.value;
faceRy = outputfaceRY.innerHTML;
}

var faceSKX = document.getElementById("faceSX");
var outputfaceSKX = document.getElementById("faceSXout");
outputfaceSKX.innerHTML = faceSKX.value;
faceSKX.oninput = function() {
  outputfaceSKX.innerHTML = this.value;
faceSkewX = outputfaceSKX.innerHTML;
}
var faceSKY = document.getElementById("faceSY");
var outputfaceSKY = document.getElementById("faceSYout");
outputfaceSKY.innerHTML = faceSKY.value;
faceSKY.oninput = function() {
  outputfaceSKY.innerHTML = this.value;
faceSkewY = outputfaceSKY.innerHTML;
}
};