///////////////   FOOT LEFT ///////////////
var footLH = '50';
var footLW = '25';
var footL3dX1 = '-25';
var footL3dY1 = '0';
var footLPer = '10000';
var footLRy = '0';
var footLRx = '0';
var footLSkewX = '0';
var footLSkewY = '0';
var footLtX = '0';
var footLtY = '0';
var footLZ = '0';
var footLrt = '0';
var footLX2 = '0';
var footLY2 = '0';
///////////////////////////////////////////
var footLfH = '25';
var footLfW = '25';
var footLf3dX1 = '-25';
var footLf3dY1 = '-9';
var footLfPer = '10000';
var footLfRy = '90';
var footLfRx = '90';
var footLfSkewX = '0';
var footLfSkewY = '0';
var footLftX = '0';
var footLftY = '0';
var footLfZ = '0';
var footLfrt = '0';
var footLfX2 = '0';
var footLfY2 = '0';
///////////////   FOOT RIGHT //////////////
var footRH = '50';
var footRW = '25';
var footR3dX1 = '25';
var footR3dY1 = '0';
var footRPer = '10000';
var footRRy = '0';
var footRRx = '0';
var footRSkewX = '0';
var footRSkewY = '0';
var footRtX = '0';
var footRtY = '0';
var footRZ = '0';
var footRrt = '0';
var footRX2 = '0';
var footRY2 = '0';
function renderObjcaracter() {
var $caracter = $("#rotation");
$caracter.append("<div id='footL'></div>")
$caracter.append("<div id='footLfront'></div>")
$caracter.append("<div id='footR'></div>")
};
var interval = setInterval(function() {
////////////////////////  FOOT LEFT
$("#footL").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'red','height': footLH, 'width': footLW,'left': footL3dX1,'top':footL3dY1,'opacity': '1','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + footLPer + 'px) rotateY(' + footLRy + 'deg) rotateX(' + footLRx + 'deg) skewX(' + footLSkewX + 'deg) skewY(' + footLSkewY + 'deg)  translate3d(' + footLtX + 'px,' + footLtY + 'px,' + footLZ + 'px)rotate(' + footLrt + 'deg)' });
$("#footLfront").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': footLfH, 'width': footLfW,'left': footLf3dX1,'top':footLf3dY1,'opacity': '1','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + footLfPer + 'px) rotateY(' + footLfRy + 'deg) rotateX(' + footLfRx + 'deg) skewX(' + footLfSkewX + 'deg) skewY(' + footLfSkewY + 'deg)  translate3d(' + footLftX + 'px,' + footLftY + 'px,' + footLfZ + 'px)rotate(' + footLfrt + 'deg)' });

////////////////////////  FOOT RIGHT
$("#footR").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'red','height': footRH, 'width': footRW,'left': footR3dX1,'top':footR3dY1,'opacity': '1','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + footRPer + 'px) rotateY(' + footRRy + 'deg) rotateX(' + footRRx + 'deg) skewX(' + footRSkewX + 'deg) skewY(' + footRSkewY + 'deg)  translate3d(' + footRtX + 'px,' + footRtY + 'px,' + footRZ + 'px)rotate(' + footRrt + 'deg)' });

////////////////////////

}, refreshStairs);