var wall3_2dW = '0%';
var wall3_2dH = '20%';
var wall3_2dX1 = '25%';
var wall3_2dY1 = '45%';
//////////wall3_1///////////
var wall3_3dX1 = '0';
var wall3_3dY1 = '0';
var wall3_Per = '2000';
var wall3_Ry = '90';
var wall3_Rx = '0';
var wall3_SkewX = '0';
var wall3_SkewY = '0';
var wall3_tX = '-35';
var wall3_tY = '0';
var wall3_Z = '20';
var wall3_rt = '0';
var wall3_X2 = '0';
var wall3_Y2 = '0';
//////////wall3_2///////////
var wall3_3dX2 = '0';
var wall3_3dY2 = '0';
var wall3_2Per = '2000';
var wall3_2Ry = '90';
var wall3_2Rx = '90';
var wall3_2SkewX = '0';
var wall3_2SkewY = '0';
var wall3_2tX = '-35';
var wall3_2tY = '0';
var wall3_2Z = '20';
var wall3_2rt = '0';
var wall3_2X2 = '0';
var wall3_2Y2 = '0';
//////////wall3_3///////////
var wall3_3dX3 = '0';
var wall3_3dY3 = '0';
var wall3_3Per = '2000';
var wall3_3Ry = '90';
var wall3_3Rx = '0';
var wall3_3SkewX = '0';
var wall3_3SkewY = '0';
var wall3_3tX = '-35';
var wall3_3tY = '0';
var wall3_3Z = '20';
var wall3_3rt = '0';
var wall3_3X2 = '0';
var wall3_3Y2 = '0';
//////////wall3_4///////////
var wall3_3dX4 = '0';
var wall3_3dY4 = '0';
var wall3_4Per = '2000';
var wall3_4Ry = '90';
var wall3_4Rx = '90';
var wall3_4SkewX = '0';
var wall3_4SkewY = '0';
var wall3_4tX = '-35';
var wall3_4tY = '0';
var wall3_4Z = '20';
var wall3_4rt = '0';
var wall3_4X2 = '0';
var wall3_4Y2 = '0';
/////////////////////////

/////////////////////////
function renderObjHouse3() {
var $house = $("#floor1");
$house.append("<div id='wall3_1'></div><div id='wall3_2'></div>")
};
$("#wall3_1_2d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'black','height': wall3_2dH, 'width': wall3_2dW,'left': wall3_2dX1,'top':wall3_2dY1,'opacity': '1', '-webkit-transform': 'rotate(' + wall3_rt + 'deg)', 'border-left': '6px solid red' });
var interval = setInterval(function() {
$("#wall3_1_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall3_3dX1,'top':wall3_3dY1,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall3_Per + 'px) rotateY(' + wall3_Ry + 'deg) rotateX(' + wall3_Rx + 'deg) skewX(' + wall3_SkewX + 'deg) skewY(' + wall3_SkewY + 'deg)  translate3d(' + wall3_tX + 'px,' + wall3_tY + 'px,' + wall3_Z + 'px)rotate(' + wall3_rt + 'deg)' });
$("#wall3_2_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall3_3dX2,'top':wall3_3dY2,'opacity': '1','background-image': 'url("../images/assets/wallF2.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall3_2Per + 'px) rotateY(' + wall3_2Ry + 'deg) rotateX(' + wall3_2Rx + 'deg) skewX(' + wall3_2SkewX + 'deg) skewY(' + wall3_2SkewY + 'deg)  translate3d(' + wall3_2tX + 'px,' + wall3_2tY + 'px,' + wall3_2Z + 'px)rotate(' + wall3_2rt + 'deg)' });
$("#wall3_3_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall3_3dX3,'top':wall3_3dY3,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall3_3Per + 'px) rotateY(' + wall3_3Ry + 'deg) rotateX(' + wall3_3Rx + 'deg) skewX(' + wall3_3SkewX + 'deg) skewY(' + wall3_3SkewY + 'deg)  translate3d(' + wall3_3tX + 'px,' + wall3_3tY + 'px,' + wall3_3Z + 'px)rotate(' + wall3_3rt + 'deg)' });
$("#wall3_4_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall3_3dX4,'top':wall3_3dY4,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall3_4Per + 'px) rotateY(' + wall3_4Ry + 'deg) rotateX(' + wall3_4Rx + 'deg) skewX(' + wall3_4SkewX + 'deg) skewY(' + wall3_4SkewY + 'deg)  translate3d(' + wall3_4tX + 'px,' + wall3_4tY + 'px,' + wall3_4Z + 'px)rotate(' + wall3_4rt + 'deg)' });
wall3_3dX1 =-rotationX + 610;
wall3_3dY1 =-rotationY + 770;
wall3_3dX2 =-rotationX + 680;
wall3_3dY2 =-rotationY + 740;
wall3_3dX3 =-rotationX + 710;
wall3_3dY3 =-rotationY + 770;
wall3_3dX4 =-rotationX + 680;
wall3_3dY4 =-rotationY + 840;
}, refreshStairs);