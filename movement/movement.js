     
// This function runs when the page is loaded
$(document).ready(function(e) {
  // Put the player in the top/middle of the screen
  // Calculate the player's "ground" position, which is the 
  // height of the stage minus the height of the player
  playerMaxTop = $("#container").height() - player_m.height();
  // Start the timer
  
  gameTimer = setInterval(update, refreshKeys);
  // Listen for keypresses


  $(document).keydown(function(e) {


      if (e.which === ZOOMPLUS) {
      zoomplusPressed = true;
//cubehight++;
//cubehight=cubehight+5;
//cubefrontH++;
//cubefrontH=cubefrontH + 5;


    } if (e.which === ZOOMMIN) {
      zoomminPressed = true;
//cubehight--;
//cubehight=cubehight-5;

cubefrontH--;
cubefrontH=cubefrontH -5;
    } if (e.which === SKEL) {
      skelPressed = true;
cubefrontW++;
cubefrontW=cubefrontW+5;

    } if (e.which === SKER) {
      skerPressed = true;
cubefrontW--;
cubefrontW=cubefrontW-5;

    } if (e.which === SPACE) {
      jumpPressed = true;
cbrtRy++;
cbrtRy=cbrtRy +5;
 
    } else if (e.which === RIGHT) {
      rightPressed = true;

//cbrtRx--;
//cbrtRx=cbrtRx -5;
    } else if (e.which === LEFT) {
      leftPressed = true;
//cbrtRx++;
//cbrtRx=cbrtRx +5;


    } else if (e.which === UP) {
      upPressed = true;

//cwrt++;
//cwrt=cwrt +5;
    } else if (e.which === DOWN) {
//      var socket = io.connect('http://localhost:8081');
      downPressed = true;
//cwrt--;
//cwrt=cwrt -5;

//socket.emit('down', 'DOWN', function (data) {
//   console.log(data); // data will be 'woot'
// });
    } else if (e.which === SHIFT) {
      shiftPressed = true;
     // speed = 20;
    } else if (e.which === DEBUG) {
      debugPressed = true;
    } else if (e.which === N2) {
      n2Pressed = true;
     
      YaxisY--;
      YaxisY = YaxisY -10;
     
    } else if (e.which === N4) {
      n4Pressed = true;
      XaxisX++;
      XaxisX = XaxisX +10;
//rRo--;
     
    } else if (e.which === N8) {
      n8Pressed = true;
     
      YaxisY++;
      YaxisY = YaxisY +10;
 

    } else if (e.which === N6) {
      n6Pressed = true;
      XaxisX--;
      XaxisX = XaxisX -10;

    }else if (e.which === CONTROL) {
      controlPressed = true;
     //CsceneMovestart();
    }

      else if (e.which === INVENTORY) {
      inventoryPressed= true;
      $( "#options" ).toggle();
      $( ".content" ).toggle();
    } else if (e.which === VAR) {
      varPressed= true;
      $( "#var1" ).toggle();
    }



  });
  $(document).keyup(function(e) {

      if (e.which === ZOOMPLUS) {
      zoomplusPressed = false;
    } if (e.which === ZOOMMIN) {
      zoomminPressed = false;

    } if (e.which === SKEL) {
      skelPressed = false;
    } if (e.which === SKER) {
      skerPressed = false;
    } if (e.which === SPACE) {
      jumpPressed = false;

XaxisY--;
XaxisY=XaxisY-10;
CameraY++;
CameraY=CameraY+10;



    } else if (e.which === RIGHT) {
      rightPressed = false;
    } else if (e.which === LEFT) {
      leftPressed = false;
    } else if (e.which === UP) {

      upPressed = false;

    } else if (e.which === DOWN) {
      downPressed = false;
    } else if (e.which === SHIFT) {
      shiftPressed = false;
      //speed = 15;
    } else if (e.which === DEBUG) {
      shiftPressed = false;
    } else if (e.which === N2) {
      n2Pressed = false;
    } else if (e.which === N4) {
      n4Pressed = false;
    } else if (e.which === N8) {
      n8Pressed = false;
    } else if (e.which === N6) {
      n6Pressed = false;
    }else if (e.which === CONTROL) {
      controlPressed = false;
     //CsceneMoveStop();
    }     else if (e.which === INVENTORY) {
      inventoryPressed = false;
    }else if (e.which === VAR) {
      varPressed = false;

    }
  });
    if (e.keyCode in map) {
        map[e.keyCode] = true;
               if (map[SHIFT] && map) {

           shiftPressed = true;
         //speed = 30;
        }
                else if (map[DEBUG] && map) {
                  debugPressed = true;
               printKeys();
        }
    }
}).keyup(function(e) {
    if (e.keyCode in map) {
        map[e.keyCode] = false;
    }

});
