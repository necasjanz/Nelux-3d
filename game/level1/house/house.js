var wall2dW = '0%';
var wall2dH = '20%';
var wall2dX1 = '25%';
var wall2dY1 = '45%';
//////////floor1///////////
var FloorH = '500';
var FloorW = '500';
var Floor3dX1 = '0';
var Floor3dY1 = '0';
var FloorPer = '10000';
var FloorRy = '0';
var FloorRx = '0';
var FloorSkewX = '0';
var FloorSkewY = '0';
var FloortX = '-45';
var FloortY = '0';
var FloorZ = '0';
var Floorrt = '0';
var FloorX2 = '0';
var FloorY2 = '0';
//////////wall1///////////
var wall1H = '2000';
var wall1W = '400';
var wall3dX1 = '0';
var wall3dY1 = '0';
var wallPer = '15000';
var wallRy = '90';
var wallRx = '0';
var wallSkewX = '0';
var wallSkewY = '0';
var walltX = '-170';
var walltY = '80';
var wallZ = '0';
var wallrt = '180';
var wallX2 = '0';
var wallY2 = '0';
//////////wall2///////////
var wall2H = '350';
var wall2W = '500';
var wall3dX2 = '0';
var wall3dY2 = '0';
var wall2Per = '10000';
var wall2Ry = '90';
var wall2Rx = '90';
var wall2SkewX = '0';
var wall2SkewY = '0';
var wall2tX = '-100';
var wall2tY = '0';
var wall2Z = '20';
var wall2rt = '90';
var wall2X2 = '0';
var wall2Y2 = '0';
//////////wall3///////////
var wall3H = '400';
var wall3W = '2000';
var wall3dX3 = '0';
var wall3dY3 = '0';
var wall3Per = '15000';
var wall3Ry = '0';
var wall3Rx = '0';
var wall3SkewX = '0';
var wall3SkewY = '0';
var wall3tX = '0';
var wall3tY = '80';
var wall3Z = '0';
var wall3rt = '90';
var wall3X2 = '0';
var wall3Y2 = '0';
//////////wall4///////////
var wall4H = '400';
var wall4W = '1000';
var wall3dX4 = '0';
var wall3dY4 = '0';
var wall4Per = '15000';
var wall4Ry = '0';
var wall4Rx = '90';
var wall4SkewX = '0';
var wall4SkewY = '0';
var wall4tX = '-90';
var wall4tY = '200';
var wall4Z = '0';
var wall4rt = '180';
var wall4X2 = '0';
var wall4Y2 = '0';
/////////////////////////
var Cealing3dX1 = '0';
var Cealing3dY1 = '0';
var CealingPer = '10000';
var CealingRy = '0';
var CealingRx = '0';
var CealingSkewX = '0';
var CealingSkewY = '0';
var CealingtX = '-100';
var CealingtY = '0';
var CealingZ = '70';
var Cealingrt = '0';
var CealingX2 = '0';
var CealingY2 = '0';
/////////////////////////
function renderObjHouse() {
var $house = $("#rotation");
$house.append("<div id='house1_Floor1'></div>")
$house.append("<div id='wall1_3d' class='.onPlatform'></div>")
$house.append("<div id='wall2_3d' class='.onPlatform'></div>")
$house.append("<div id='wall3_3d' class='.onPlatform'></div>")
$house.append("<div id='wall4_3d' class='.onPlatform'></div>")
$house.append("<div id='house1_Cealing1'></div>")



};
$("#wall1_2d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'black','height': wall2dH, 'width': wall2dW,'left': wall2dX1,'top':wall2dY1,'opacity': '1', '-webkit-transform': 'rotate(' + wallrt + 'deg)', 'border-left': '6px solid red' });

var interval = setInterval(function() {
$("#house1_Floor1").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': FloorH, 'width': FloorW,'left': Floor3dX1,'top':Floor3dY1,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + FloorPer + 'px) rotateY(' + FloorRy + 'deg) rotateX(' + FloorRx + 'deg) skewX(' + FloorSkewX + 'deg) skewY(' + FloorSkewY + 'deg)  translate3d(' + FloortX + 'px,' + FloortY + 'px,' + FloorZ + 'px)rotate(' + Floorrt + 'deg)' });

$("#wall1_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': wall1H, 'width': wall1W,'left': wall3dX1,'top':wall3dY1,'opacity': '1','background-image': 'url("../images/assets/wallF2.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wallPer + 'px) rotateY(' + wallRy + 'deg) rotateX(' + wallRx + 'deg) skewX(' + wallSkewX + 'deg) skewY(' + wallSkewY + 'deg)  translate3d(' + walltX + 'px,' + walltY + 'px,' + wallZ + 'px)rotate(' + wallrt + 'deg)' });
$("#wall2_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': wall2H, 'width': wall2W,'left': wall3dX2,'top':wall3dY2,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall2Per + 'px) rotateY(' + wall2Ry + 'deg) rotateX(' + wall2Rx + 'deg) skewX(' + wall2SkewX + 'deg) skewY(' + wall2SkewY + 'deg)  translate3d(' + wall2tX + 'px,' + wall2tY + 'px,' + wall2Z + 'px)rotate(' + wall2rt + 'deg)' });
$("#wall3_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': wall3H, 'width': wall3W,'left': wall3dX3,'top':wall3dY3,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall3Per + 'px) rotateY(' + wall3Ry + 'deg) rotateX(' + wall3Rx + 'deg) skewX(' + wall3SkewX + 'deg) skewY(' + wall3SkewY + 'deg)  translate3d(' + wall3tX + 'px,' + wall3tY + 'px,' + wall3Z + 'px)rotate(' + wall3rt + 'deg)' });
$("#wall4_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': wall4H, 'width': wall4W,'left': wall3dX4,'top':wall3dY4,'opacity': '1','background-image': 'url("../images/brickwall.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall4Per + 'px) rotateY(' + wall4Ry + 'deg) rotateX(' + wall4Rx + 'deg) skewX(' + wall4SkewX + 'deg) skewY(' + wall4SkewY + 'deg)  translate3d(' + wall4tX + 'px,' + wall4tY + 'px,' + wall4Z + 'px)rotate(' + wall4rt + 'deg)' });

$("#house1_Cealing1").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'blue','height': '100px', 'width': '100px','left': Cealing3dX1,'top':Cealing3dY1,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + CealingPer + 'px) rotateY(' + CealingRy + 'deg) rotateX(' + CealingRx + 'deg) skewX(' + CealingSkewX + 'deg) skewY(' + CealingSkewY + 'deg)  translate3d(' + CealingtX + 'px,' + CealingtY + 'px,' + CealingZ + 'px)rotate(' + Cealingrt + 'deg)' });


Floor3dX1 =-rotationX + 190;
Floor3dY1 =-rotationY + 1000;

wall3dX3 =-rotationX + 1000;//right wall
wall3dY3 =-rotationY + 800;//right wall

wall3dX2 =-rotationX + 150;//top wall
wall3dY2 =-rotationY + -30;//top wall

wall3dX1 =-rotationX + -900;//left wall
wall3dY1 =-rotationY + 600;//left wall

wall3dX4 =-rotationX + 135;
wall3dY4 =-rotationY + 1600;

Cealing3dX1 =-rotationX + 210;
Cealing3dY1 =-rotationY + 170;


}, refreshStairs);