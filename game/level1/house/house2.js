var wall2_2dW = '0%';
var wall2_2dH = '20%';
var wall2_2dX1 = '25%';
var wall2_2dY1 = '45%';
//////////wall2_1///////////
var wall2_3dX1 = '0';
var wall2_3dY1 = '0';
var wall2_Per = '2000';
var wall2_Ry = '90';
var wall2_Rx = '0';
var wall2_SkewX = '0';
var wall2_SkewY = '0';
var wall2_tX = '-35';
var wall2_tY = '0';
var wall2_Z = '20';
var wall2_rt = '0';
var wall2_X2 = '0';
var wall2_Y2 = '0';
//////////wall2_2///////////
var wall2_3dX2 = '0';
var wall2_3dY2 = '0';
var wall2_2Per = '2000';
var wall2_2Ry = '90';
var wall2_2Rx = '90';
var wall2_2SkewX = '0';
var wall2_2SkewY = '0';
var wall2_2tX = '-35';
var wall2_2tY = '0';
var wall2_2Z = '20';
var wall2_2rt = '0';
var wall2_2X2 = '0';
var wall2_2Y2 = '0';
//////////wall2_3///////////
var wall2_3dX3 = '0';
var wall2_3dY3 = '0';
var wall2_3Per = '2000';
var wall2_3Ry = '90';
var wall2_3Rx = '0';
var wall2_3SkewX = '0';
var wall2_3SkewY = '0';
var wall2_3tX = '-35';
var wall2_3tY = '0';
var wall2_3Z = '20';
var wall2_3rt = '0';
var wall2_3X2 = '0';
var wall2_3Y2 = '0';
//////////wall2_4///////////
var wall2_3dX4 = '0';
var wall2_3dY4 = '0';
var wall2_4Per = '2000';
var wall2_4Ry = '90';
var wall2_4Rx = '90';
var wall2_4SkewX = '0';
var wall2_4SkewY = '0';
var wall2_4tX = '-35';
var wall2_4tY = '0';
var wall2_4Z = '20';
var wall2_4rt = '0';
var wall2_4X2 = '0';
var wall2_4Y2 = '0';
/////////////////////////

/////////////////////////
function renderObjHouse2() {
var $house = $("#floor1");
$house.append("<div id='wall2_1'></div><div id='wall2_2'></div>")
};
$("#wall2_1_2d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': 'black','height': wall2_2dH, 'width': wall2_2dW,'left': wall2_2dX1,'top':wall2_2dY1,'opacity': '1', '-webkit-transform': 'rotate(' + wall2_rt + 'deg)', 'border-left': '6px solid red' });

var interval = setInterval(function() {

$("#wall2_1_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall2_3dX1,'top':wall2_3dY1,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall2_Per + 'px) rotateY(' + wall2_Ry + 'deg) rotateX(' + wall2_Rx + 'deg) skewX(' + wall2_SkewX + 'deg) skewY(' + wall2_SkewY + 'deg)  translate3d(' + wall2_tX + 'px,' + wall2_tY + 'px,' + wall2_Z + 'px)rotate(' + wall2_rt + 'deg)' });
$("#wall2_2_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall2_3dX2,'top':wall2_3dY2,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall2_2Per + 'px) rotateY(' + wall2_2Ry + 'deg) rotateX(' + wall2_2Rx + 'deg) skewX(' + wall2_2SkewX + 'deg) skewY(' + wall2_2SkewY + 'deg)  translate3d(' + wall2_2tX + 'px,' + wall2_2tY + 'px,' + wall2_2Z + 'px)rotate(' + wall2_2rt + 'deg)' });
$("#wall2_3_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall2_3dX3,'top':wall2_3dY3,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall2_3Per + 'px) rotateY(' + wall2_3Ry + 'deg) rotateX(' + wall2_3Rx + 'deg) skewX(' + wall2_3SkewX + 'deg) skewY(' + wall2_3SkewY + 'deg)  translate3d(' + wall2_3tX + 'px,' + wall2_3tY + 'px,' + wall2_3Z + 'px)rotate(' + wall2_3rt + 'deg)' });
$("#wall2_4_3d").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '100px', 'width': '70px','left': wall2_3dX4,'top':wall2_3dY4,'opacity': '1','background-image': 'url("../images/assets/wallF2.png")','background-size': '100% 100%','transform-style': 'preserve-3d','transform-style': 'preserve-3d', '-webkit-transform': 'perspective(' + wall2_4Per + 'px) rotateY(' + wall2_4Ry + 'deg) rotateX(' + wall2_4Rx + 'deg) skewX(' + wall2_4SkewX + 'deg) skewY(' + wall2_4SkewY + 'deg)  translate3d(' + wall2_4tX + 'px,' + wall2_4tY + 'px,' + wall2_4Z + 'px)rotate(' + wall2_4rt + 'deg)' });



//$("#wall2_2").css({  'z-index':'5','position': 'absolute','overflow': 'hidden','background-color': '','height': '27%', 'width': '1%','left': wall2_X2,'top':'49%','opacity': '1','background-image': 'url("../images/assets/wall2_.png")','background-size': '100% 100%','transform-style': 'preserve-3d' });
//$("#room3").css({  'position': 'absolute','overflow': 'hidden','background-color': '','height': scaleY, 'width': scaleX,'right': top2X,'top':top2Y,'opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%', 'transform': 'perspective: 200px', 'transform-style': 'preserve-3d'});
//$("#room3F").css({  'z-index':'7','position': 'absolute','overflow': 'hidden','background-color': '','height': scale1Y, 'width': scale1X,'left': top1X,'top':top1Y,'opacity': '1','background-image': 'url("../images/assets/wall2_F2.png")','background-size': '100% 100%', 'transform': 'perspective: 200px', 'transform-style': 'preserve-3d'});
//$("#basement").css({  'z-index':'0','position': 'absolute','overflow': 'hidden','background-color': '','height': scale2Y, 'width': scale2X,'left': top3X,'top':top3Y,'opacity': '1','background-image': 'url("../images/assets/back_wall2_.png")','background-size': '100% 100%', 'transform': 'perspective: 200px', 'transform-style': 'preserve-3d'});

//$("#cealing").css({  'position': 'absolute','overflow': 'hidden','background-color': '','height': '16%', 'width': '100%','right': '50%','top':'0%','opacity': '1','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%', 'transform': 'rotateX(45deg) perspective(100px)', 'transform-style': 'preserve-3d' });
//$("#wall2_L").css({  'z-index':'2','position': 'absolute','overflow': 'hidden','background-color': '','height': wall2_LSizY, 'width': wall2_LSizX,'left': wall2_LPoX,'top': wall2_LPoY, 'transform': 'perspective(' + wall2_LP + 'px) rotateY(' + wall2_Lbasedegy + 'deg) rotateX(' + wall2_Lbasedegx + 'deg) skewY(' + wall2_LS + 'deg)','background-image': 'url("../images/assets/wood1.png")','background-size': '100% 100%', 'transform-style': 'preserve-3d' });
//$("#wall2_R").css({  'position': 'absolute','overflow': 'hidden','background-color': '','height': wall2_RSizY, 'width': wall2_RSizX,'left': wall2_RPoX,'top':wall2_RPoY, 'transform': 'perspective(' + wall2_RP + 'px) rotateY(' + wall2_Rbasedegy + 'deg) rotateX(' + wall2_Rbasedegx + 'deg)','background-image': 'url("../images/assets/wall2__door.png")','background-size': '100% 100%', 'transform-style': 'preserve-3d' });
wall2_3dX1 =-rotationX + 310;
wall2_3dY1 =-rotationY + 170;
wall2_3dX2 =-rotationX + 380;
wall2_3dY2 =-rotationY + 140;
wall2_3dX3 =-rotationX + 410;
wall2_3dY3 =-rotationY + 170;
wall2_3dX4 =-rotationX + 380;
wall2_3dY4 =-rotationY + 240;
}, refreshStairs);